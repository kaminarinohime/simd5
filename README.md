simd5 is a functional prototype that compute MD5 hash of multiple files using a SIMD approach.

md5.c is a MD5 reference for a single file while md5_simd.c is the source for the SIMD version.
