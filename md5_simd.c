#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <immintrin.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)


const uint32_t BUF_SIZE = 1<<12;

const uint32_t S[] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                       5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
                       4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                       6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};

const uint32_t K[] = {0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                       0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                       0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                       0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                       0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                       0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                       0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                       0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                       0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                       0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                       0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                       0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                       0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                       0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                       0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                       0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};

typedef struct{
    __m256i A;
    __m256i B;
    __m256i C;
    __m256i D;
}MD5hash;


typedef struct{
    uint8_t pad[128];
    FILE *fid;
    uint8_t* mmap;
    uint64_t size;
    uint64_t mod;
    uint64_t pos;
}File;


void MD5Init(MD5hash* md5)
{
    md5->A = _mm256_set1_epi32(0x67452301);
    md5->B = _mm256_set1_epi32(0xefcdab89);
    md5->C = _mm256_set1_epi32(0x98badcfe);
    md5->D = _mm256_set1_epi32(0x10325476);
}


void initFile(File* file, const char* name)
{
    struct stat sb;

    const int fd = open(name, O_RDONLY);
    fstat(fd, &sb);
    close(fd);

    file->pos = 0;
    file->fid = fopen(name, "r");
    file->size = sb.st_size;

    file->mmap = malloc(sizeof(uint8_t)*BUF_SIZE);

    fread(file->mmap, sizeof(uint8_t), (file->size<=BUF_SIZE) ? file->size : BUF_SIZE, file->fid);


    const uint64_t bg_size = 8*file->size;
    const uint64_t mes_chunck = file->size/64;
    const uint64_t res_len = file->size%64;

    file->mod = res_len<=56 ? 56 : 120;

    const uint64_t pad_len = file->mod - res_len;

    const long fpos = ftell(file->fid);
    rewind(file->fid);
    fseek(file->fid, 64*mes_chunck, SEEK_SET);
    fread(file->pad, sizeof(uint8_t), res_len, file->fid);
    rewind(file->fid);
    fseek(file->fid, fpos, SEEK_SET);

    file->pad[res_len] = 0x80;
    memset(file->pad+res_len+1, 0, sizeof(uint8_t)*pad_len);
    memcpy(file->pad+file->mod, &bg_size, sizeof(uint64_t));

}

void freeFile(File *file)
{
    free(file->mmap);
    file->mmap = NULL;

    fclose(file->fid);
}

__m256i leftRotate(const __m256i x, const uint32_t n)
{
    __m256i left = _mm256_slli_epi32(x, n);
    __m256i right = _mm256_srli_epi32(x, 32-n);

    return _mm256_or_si256(left, right);
}


inline void MD5Block1(__m256i *A, __m256i *B, __m256i *C, __m256i *D, const uint32_t *msg, const uint32_t k, const uint32_t S)
{

     __m256i vmsg;
    memcpy(&vmsg, msg, sizeof(__m256i));

    const __m256i K = _mm256_set1_epi32(k);
    const __m256i f1 = _mm256_and_si256(*B,*C);
    const __m256i f2 = _mm256_andnot_si256(*B,*D);
    const __m256i tmp = _mm256_add_epi32(*A, _mm256_add_epi32(K, vmsg));
    __m256i F = _mm256_or_si256(f1, f2);

    F = _mm256_add_epi32(F, tmp);
    *A = *D;
    *D = *C;
    *C = *B;
    *B = _mm256_add_epi32(*B, leftRotate(F,S));


}

inline void MD5Block2(__m256i *A, __m256i *B, __m256i *C, __m256i *D, const uint32_t *msg, const uint32_t k, const uint32_t S)
{
     __m256i vmsg;
    memcpy(&vmsg, msg, sizeof(__m256i));

    const __m256i K = _mm256_set1_epi32(k);
    const __m256i f1 = _mm256_and_si256(*D,*B);
    const __m256i f2 = _mm256_andnot_si256(*D,*C);
    const __m256i tmp = _mm256_add_epi32(*A, _mm256_add_epi32(K, vmsg));
    __m256i F = _mm256_or_si256(f1, f2);

    F = _mm256_add_epi32(F, tmp);
    *A = *D;
    *D = *C;
    *C = *B;
    *B = _mm256_add_epi32(*B, leftRotate(F,S));

}


inline void MD5Block3(__m256i *A, __m256i *B, __m256i *C, __m256i *D, const  uint32_t *msg, const uint32_t k, const uint32_t S)
{

    __m256i vmsg;
    memcpy(&vmsg, msg, sizeof(__m256i));

    const __m256i K = _mm256_set1_epi32(k);
    const __m256i f1 = _mm256_xor_si256(*B,*C);
    const __m256i tmp = _mm256_add_epi32(*A, _mm256_add_epi32(K, vmsg));
    __m256i F = _mm256_xor_si256(f1, *D);

     F = _mm256_add_epi32(F, tmp);
    *A = *D;
    *D = *C;
    *C = *B;
    *B = _mm256_add_epi32(*B, leftRotate(F,S));

}


inline void MD5Block4(__m256i *A, __m256i *B, __m256i *C, __m256i *D, const  uint32_t *msg, const uint32_t k, const uint32_t S)
{

    const __m256i NOT = _mm256_set1_epi8(0xFF);
    const __m256i K = _mm256_set1_epi32(k);

    __m256i vmsg;
    memcpy(&vmsg, msg, sizeof(__m256i));

    const __m256i f1 = _mm256_xor_si256(*D, NOT);
    const __m256i tmp = _mm256_add_epi32(*A, _mm256_add_epi32(K, vmsg));
    __m256i F = _mm256_or_si256(*B, f1);
    F = _mm256_xor_si256(*C, F);

    F = _mm256_add_epi32(F, tmp);
    *A = *D;
    *D = *C;
    *C = *B;
    *B = _mm256_add_epi32(*B, leftRotate(F,S));

}

inline __m256i computeMask(const uint8_t mask_eof)
{
    const __m256i ROT = _mm256_set_epi32(0,1,2,3,4,5,6,7);
    const __m256i MASK = _mm256_set1_epi32(1);

    __m256i mask = _mm256_set1_epi32(mask_eof);
    mask = _mm256_srlv_epi32 (mask, ROT);

    return _mm256_and_si256(mask, MASK);
}


inline __m256i movemask8(const uint8_t mask)
{
    const __m256i shift_mask = _mm256_setr_epi32(1<<0, 1<<1, 1<<2, 1<<3, 1<<4, 1<<5, 1<<6, 1<<7);

    __m256i mask8 = _mm256_set1_epi32(mask);
    mask8 = _mm256_and_si256(mask8, shift_mask);
    mask8 = _mm256_cmpgt_epi32(mask8, _mm256_setzero_si256());

    return mask8;
}


inline __m256i __mm256_blend_epi32(const __m256i A, const __m256i A1, const __m256i mask)
{

    __m256 Aps = _mm256_castsi256_ps(A);
    __m256 A1ps = _mm256_castsi256_ps(A1);
    __m256 maskps =  _mm256_castsi256_ps(mask);

    return _mm256_castps_si256(_mm256_blendv_ps(Aps, A1ps, maskps));

}


void MD5Rounds(MD5hash *md5, const uint32_t *mat_chunk, const uint8_t mask_eof)
{
    const __m256i vectormask = movemask8(mask_eof);

    MD5hash tmp;
    memcpy(&tmp, md5, sizeof(MD5hash));

    //Sugar syntax
    __m256i A = md5->A;
    __m256i B = md5->B;
    __m256i C = md5->C;
    __m256i D = md5->D;

    MD5Block1(&A, &B, &C, &D, mat_chunk+0, K[0], S[0]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+8, K[1], S[1]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+16, K[2], S[2]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+24, K[3], S[3]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+32, K[4], S[4]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+40, K[5], S[5]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+48, K[6], S[6]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+56, K[7], S[7]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+64, K[8], S[8]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+72, K[9], S[9]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+80, K[10], S[10]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+88, K[11], S[11]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+96, K[12], S[12]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+104, K[13], S[13]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+112, K[14], S[14]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+120, K[15], S[15]);


    MD5Block2(&A, &B, &C, &D, mat_chunk+8, K[16], S[16]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+48, K[17], S[17]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+88, K[18], S[18]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+0, K[19], S[19]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+40, K[20], S[20]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+80, K[21], S[21]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+120, K[22], S[22]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+32, K[23], S[23]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+72, K[24], S[24]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+112, K[25], S[25]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+24, K[26], S[26]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+64, K[27], S[27]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+104, K[28], S[28]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+16, K[29], S[29]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+56, K[30], S[30]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+96, K[31], S[31]);


    MD5Block3(&A, &B, &C, &D, mat_chunk+40, K[32], S[32]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+64, K[33], S[33]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+88, K[34], S[34]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+112, K[35], S[35]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+8, K[36], S[36]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+32, K[37], S[37]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+56, K[38], S[38]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+80, K[39], S[39]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+104, K[40], S[40]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+0, K[41], S[41]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+24, K[42], S[42]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+48, K[43], S[43]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+72, K[44], S[44]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+96, K[45], S[45]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+120, K[46], S[46]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+16, K[47], S[47]);


    MD5Block4(&A, &B, &C, &D, mat_chunk+0, K[48], S[48]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+56, K[49], S[49]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+112, K[50], S[50]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+40, K[51], S[51]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+96, K[52], S[52]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+24, K[53], S[53]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+80, K[54], S[54]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+8, K[55], S[55]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+64, K[56], S[56]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+120, K[57], S[57]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+48, K[58], S[58]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+104, K[59], S[59]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+32, K[60], S[60]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+88, K[61], S[61]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+16, K[62], S[62]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+72, K[63], S[63]);


    A = _mm256_add_epi32(A, md5->A);
    B = _mm256_add_epi32(B, md5->B);
    C = _mm256_add_epi32(C, md5->C);
    D = _mm256_add_epi32(D, md5->D);


    A =  __mm256_blend_epi32(A, tmp.A, vectormask);
    B =  __mm256_blend_epi32(B, tmp.B, vectormask);
    C =  __mm256_blend_epi32(C, tmp.C, vectormask);
    D =  __mm256_blend_epi32(D, tmp.D, vectormask);


    md5->A = A;
    md5->B = B;
    md5->C = C;
    md5->D = D;

}


int main(int argc, char *argv[])
{

    const uint32_t num_file = argc-1;
    const uint32_t AVX_BLOCK_SIZE = sizeof(__m256i)/sizeof(uint32_t);

    File files[AVX_BLOCK_SIZE];

    MD5hash md5;

    uint32_t *mat_chunk = malloc(sizeof(uint32_t)*AVX_BLOCK_SIZE*16); //16*4B = 512 bits

    for(int i=0; i<num_file; i+=AVX_BLOCK_SIZE)
    {
        uint32_t sub_num_file = (i+AVX_BLOCK_SIZE)>num_file ? num_file-i:AVX_BLOCK_SIZE;
        uint8_t mask_eof = -1;
        uint8_t cp_mask_eof, ref_mask_eof;

        for(int j=0; j<sub_num_file; j++)
        {
            initFile(files+j, argv[i+1+j]);
            mask_eof &= ~(1<<j); //Put the bit to 0
        }

        cp_mask_eof = mask_eof;
        ref_mask_eof = mask_eof;

        uint64_t hm_pos[sub_num_file];
        memset(hm_pos, 0, sizeof(hm_pos));

        MD5Init(&md5);

        while(mask_eof!=(uint8_t)-1)
        {
            for(int j=0; j<16; j++)
            {
                for(int k=0; k<sub_num_file; k++)
                {
                    if((mask_eof & (1<<k))==0 && files[k].size/64>0 && files[k].pos<files[k].size) //Check bit value
                    {
                            memcpy(mat_chunk + 8*j+k, files[k].mmap + hm_pos[k], sizeof(uint32_t));
                            hm_pos[k] += sizeof(uint32_t);
                            files[k].pos += sizeof(uint32_t);

                            if(unlikely(files[k].pos%BUF_SIZE==0))
                            {
                                uint64_t tmp = files[k].size - files[k].pos;
                                tmp = tmp>BUF_SIZE ? BUF_SIZE : tmp;

                                fread(files[k].mmap, sizeof(uint8_t), tmp, files[k].fid);
                                hm_pos[k] = 0;
                            }
                    }
                    else
                        mask_eof |= (1<<k);
                }
            }

            MD5Rounds(&md5, mat_chunk, mask_eof);
        }

        for(int k=0; k<sub_num_file; k++) //May be optimised in the above else condition
            files[k].pos = 0;

        mask_eof = ref_mask_eof;
        cp_mask_eof = ref_mask_eof;

        while(mask_eof!=(uint8_t)-1)
        {
            for(int j=0; j<16; j++)
            {
                for(int k=0; k<sub_num_file; k++)
                {
                    if((mask_eof & (1<<k))==0)
                    {
                        memcpy(mat_chunk + 8*j+k, files[k].pad + files[k].pos, sizeof(uint32_t));
                        files[k].pos += sizeof(uint32_t);

                        if(files[k].pos >= (files[k].mod+8))
                            cp_mask_eof |= (1<<k);

                    }
                }
            }

             MD5Rounds(&md5, mat_chunk, mask_eof);
             mask_eof = cp_mask_eof;

        }


        uint32_t a[8];
        uint32_t b[8];
        uint32_t c[8];
        uint32_t d[8];

        memcpy(a, &md5.A, sizeof(__m256i));
        memcpy(b, &md5.B, sizeof(__m256i));
        memcpy(c, &md5.C, sizeof(__m256i));
        memcpy(d, &md5.D, sizeof(__m256i));

        for(int j=0; j<sub_num_file; j++)
        {
            a[j] = _bswap(a[j]);
            b[j] = _bswap(b[j]);
            c[j] = _bswap(c[j]);
            d[j] = _bswap(d[j]);

            printf("%s: %08x%08x%08x%08x\n", argv[i+j+1], a[j], b[j], c[j], d[j]);
            freeFile(files+j);
        }

    }


    free(mat_chunk);
    mat_chunk = NULL;

}
