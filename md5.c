#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <immintrin.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)


const uint32_t BUF_SIZE = 1<<12; //4096B


const uint32_t S[] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                       5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
                       4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                       6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};


const uint32_t K[] = {0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                       0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                       0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                       0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                       0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                       0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                       0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                       0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                       0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                       0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                       0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                       0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                       0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                       0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                       0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                       0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};



typedef struct{
    uint32_t A;
    char pad[60];
    uint32_t B;
    char pad1[60];
    uint32_t C;
    char pad2[60];
    uint32_t D;
    char pad3[60];
}MD5hash;


typedef struct{
    uint8_t pad[128];
    FILE *fid;
    uint8_t* mmap;
    uint64_t size;
    uint64_t pos;
    uint64_t mod;
}File;



void MD5Init(MD5hash* md5)
{
    md5->A = 0x67452301;
    md5->B = 0xefcdab89;
    md5->C = 0x98badcfe;
    md5->D = 0x10325476;
}

void initFile(File* file, const char* name)
{
    struct stat sb;

    const int fd = open(name, O_RDONLY);
    fstat(fd, &sb);
    close(fd);

    file->pos = 0;
    file->fid = fopen(name, "r");
    file->size = sb.st_size;

    file->mmap = malloc(sizeof(uint8_t)*BUF_SIZE);

    fread(file->mmap, sizeof(uint8_t), (file->size<=BUF_SIZE) ? file->size : BUF_SIZE, file->fid);

    const uint64_t bg_size = 8*file->size;
    const uint64_t mes_chunck = file->size/64;
    const uint64_t res_len = file->size%64;

    file->mod = res_len<=56 ? 56 : 120;

    const uint64_t pad_len = file->mod - res_len;

    const long fpos = ftell(file->fid);
    rewind(file->fid);
    fseek(file->fid, 64*mes_chunck, SEEK_SET);
    fread(file->pad, sizeof(uint8_t), res_len, file->fid);
    rewind(file->fid);
    fseek(file->fid, fpos, SEEK_SET);

    file->pad[res_len] = 0x80;
    memset(file->pad+res_len+1, 0, sizeof(uint8_t)*pad_len);
    memcpy(file->pad+file->mod, &bg_size, sizeof(uint64_t));

}

void freeFile(File *file)
{
    free(file->mmap);
    file->mmap = NULL;

    fclose(file->fid);
    file->fid = NULL;
}


inline uint32_t leftRotate(const uint32_t x, const uint32_t n)
{
    uint32_t left = x << n;
    uint32_t right = x >> (32-n);

    return left | right;
}


inline void MD5Block1(uint32_t *A, uint32_t *B, uint32_t *C, uint32_t *D, const uint32_t *msg, const uint32_t K, const uint32_t S)
{
    uint32_t F = (*B & *C) | ((~(*B)) & *D);

     F += *A + K + *msg;
    *A = *D;
    *D = *C;
    *C = *B;
    *B += leftRotate(F, S);


}

inline void MD5Block2(uint32_t *A, uint32_t *B, uint32_t *C, uint32_t *D, const uint32_t *msg, const uint32_t K, const uint32_t S)
{

    uint32_t F = (*D & *B) | ((~(*D)) & *C);
    F += *A + K + *msg;
    *A = *D;
    *D = *C;
    *C = *B;
    *B += leftRotate(F, S);

}


inline void MD5Block3(uint32_t *A, uint32_t *B, uint32_t *C, uint32_t *D, const uint32_t *msg, const uint32_t K, const uint32_t S)
{

    uint32_t F = *B ^ *C ^ *D;

    F += *A + K + *msg;
    *A = *D;
    *D = *C;
    *C = *B;
    *B += leftRotate(F, S);

}


inline void MD5Block4(uint32_t *A, uint32_t *B, uint32_t *C, uint32_t *D, const uint32_t *msg, const uint32_t K, const uint32_t S)
{

    uint32_t F = *C ^ (*B | ~(*D));
    F += *A + K + *msg;
    *A = *D;
    *D = *C;
    *C = *B;
    *B += leftRotate(F, S);

}



void MD5Rounds(MD5hash *md5, const uint8_t *bg_mat_chunk)
{
    uint32_t *mat_chunk = (uint32_t*) bg_mat_chunk;

    //Sugar syntax
    uint32_t A = md5->A;
    uint32_t B = md5->B;
    uint32_t C = md5->C;
    uint32_t D = md5->D;


    MD5Block1(&A, &B, &C, &D, mat_chunk+0, K[0], S[0]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+1, K[1], S[1]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+2, K[2], S[2]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+3, K[3], S[3]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+4, K[4], S[4]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+5, K[5], S[5]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+6, K[6], S[6]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+7, K[7], S[7]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+8, K[8], S[8]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+9, K[9], S[9]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+10, K[10], S[10]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+11, K[11], S[11]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+12, K[12], S[12]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+13, K[13], S[13]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+14, K[14], S[14]);
    MD5Block1(&A, &B, &C, &D, mat_chunk+15, K[15], S[15]);


    MD5Block2(&A, &B, &C, &D, mat_chunk+1, K[16], S[16]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+6, K[17], S[17]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+11, K[18], S[18]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+0, K[19], S[19]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+5, K[20], S[20]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+10, K[21], S[21]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+15, K[22], S[22]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+4, K[23], S[23]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+9, K[24], S[24]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+14, K[25], S[25]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+3, K[26], S[26]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+8, K[27], S[27]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+13, K[28], S[28]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+2, K[29], S[29]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+7, K[30], S[30]);
    MD5Block2(&A, &B, &C, &D, mat_chunk+12, K[31], S[31]);


    MD5Block3(&A, &B, &C, &D, mat_chunk+5, K[32], S[32]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+8, K[33], S[33]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+11, K[34], S[34]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+14, K[35], S[35]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+1, K[36], S[36]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+4, K[37], S[37]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+7, K[38], S[38]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+10, K[39], S[39]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+13, K[40], S[40]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+0, K[41], S[41]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+3, K[42], S[42]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+6, K[43], S[43]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+9, K[44], S[44]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+12, K[45], S[45]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+15, K[46], S[46]);
    MD5Block3(&A, &B, &C, &D, mat_chunk+2, K[47], S[47]);


    MD5Block4(&A, &B, &C, &D, mat_chunk+0, K[48], S[48]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+7, K[49], S[49]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+14, K[50], S[50]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+5, K[51], S[51]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+12, K[52], S[52]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+3, K[53], S[53]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+10, K[54], S[54]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+1, K[55], S[55]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+8, K[56], S[56]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+15, K[57], S[57]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+6, K[58], S[58]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+13, K[59], S[59]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+4, K[60], S[60]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+11, K[61], S[61]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+2, K[62], S[62]);
    MD5Block4(&A, &B, &C, &D, mat_chunk+9, K[63], S[63]);

    md5->A += A;
    md5->B += B;
    md5->C += C;
    md5->D += D;

}



int main(int argc, char *argv[])
{
    MD5hash md5;
    File file;

    initFile(&file, argv[1]);

    MD5Init(&md5);

    uint64_t hm_pos = 0;

    for(uint64_t i=0; i<file.size/64; i++)
    {
        MD5Rounds(&md5, file.mmap+hm_pos);

        hm_pos += 64;
        file.pos += 64;

        if(unlikely(file.pos%BUF_SIZE==0))
        {
            uint64_t tmp = file.size - file.pos;
            tmp = tmp>BUF_SIZE ? BUF_SIZE : tmp;

            fread(file.mmap, sizeof(uint8_t), tmp, file.fid);
            hm_pos = 0;
        }

    }

    for(uint64_t i=0; i<file.mod+8; i+=64)
        MD5Rounds(&md5, file.pad+i);


    md5.A = _bswap(md5.A);
    md5.B = _bswap(md5.B);
    md5.C = _bswap(md5.C);
    md5.D = _bswap(md5.D);

    printf("%s: %08x%08x%08x%08x\n", argv[1], md5.A, md5.B, md5.C, md5.D);


    freeFile(&file);
}


